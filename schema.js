const { GraphQLObjectType, GraphQLString, GraphQLList, GraphQLSchema, GraphQLFloat } = require("graphql");
const axios = require('axios');
const { MongoClient } = require('mongodb');
const context = () => MongoClient.connect('mongodb+srv://absalem:vrcfCAPsJdnFBdVL@cluster0-y3okr.mongodb.net/playground', { useUnifiedTopology: true }).then(client => client.db('playground'));

const StateType = new GraphQLObjectType({
  name: "State",
  fields: () => ({
      id: {type :GraphQLFloat},
    name: { type: GraphQLString },
    desc: { type: GraphQLString },
  }),
});


//Root Query
const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        statesASC: {
            type: new GraphQLList(StateType),
            resolve(parent, args){
                return context().then(db => db.collection('us_states').find().sort({ name: 1}).toArray())
            }
        },
        statesDSC: {
            type: new GraphQLList(StateType),
            resolve(parent, args){
                return context().then(db => db.collection('us_states').find().sort({ name: -1}).toArray())
            }
        }
    }

});

module.exports = new GraphQLSchema({
    query: RootQuery
})