import React from 'react';
import './App.css';
import ApolloClient from 'apollo-boost';
import {ApolloProvider} from '@apollo/react-hooks';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

import './components/States'
import States from "./components/States";
const client = new ApolloClient({
    uri: 'http://localhost:5000/graphql',
});


function App() {
    return (<ApolloProvider client={client}>
        <div style={{width: '80%', margin: 'auto'}}>
        <Router>
            <div>
                <nav className={`my-3`}>
                    <Link to="/" className={`btn btn-primary`} style={{ 'textDecoration': 'none' }}>States A-Z</Link>
                    <Link to="/states_dsc" className={`btn btn-primary ml-3`}>States Z-A</Link>
                </nav>

                <Switch>
                    <Route exact path="/">
                        <States order={"ASC"}/>
                    </Route>
                    <Route path="/states_dsc">
                        <States order={"DSC"}/>
                    </Route>
                </Switch>
            </div>
        </Router>
        </div>
    </ApolloProvider>);
}

export default App;
