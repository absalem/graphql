import React from 'react';
import {buildResolveInfo} from "graphql/execution/execute";

function State(props) {
    return (
        <tr className={`my-3`}>
            <td className={`state`}>
                {props.stateName}
            </td>
            <td>
                {props.stateDesc}
                <br/>

            </td>
        </tr>
    );
}


export default State;