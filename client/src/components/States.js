import React from 'react';
import {gql} from "apollo-boost";
import {useQuery} from '@apollo/react-hooks';
import State from './State'

const STATES_QUERY_ASC = gql`
     query StatesQuery {
        statesASC{
          name
          desc
        }
      }
    `;

const STATES_QUERY_DSC = gql`
     query StatesQuery {
        statesDSC{
          name
          desc
        }
      }
    `;

function States(props) {
    let query, dataObject;
    props.order === "ASC" ? query=STATES_QUERY_ASC : query=STATES_QUERY_DSC;
    props.order === "ASC" ? dataObject="statesASC" : dataObject="statesDSC"
    const {loading, error, data} = useQuery(query);
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;
    return (
        <div>
        <h1 className={`my-5`}>The States</h1>
        <table>
            <tbody>
            {data[dataObject].map(({name, desc}, index) => (
                <State key={index} index={index} stateName={name} stateDesc={desc}/>))}
            </tbody>

        </table>

    </div>)

}


export default States;